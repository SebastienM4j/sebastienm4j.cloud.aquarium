Aquarium
========

Serveur pour SonarQube.


Installation
------------

* Installer [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) >= 2.5
* Installer les dépendances :
    ```shell
    ansible-galaxy install -r requirements.yml
    ```

Usage
-----

> Prévu par défaut pour des machines OVH de type Ubuntu 18.04.

Pour l'installation sur une instance du cloud public OVH :

```shell
ansible-playbook -i inventory --limit "aquarium-cloud" -e "ansible_host={ip}" --ask-vault-pass aquarium.yml
```

Pour l'installation sur le VPS OVH :

```shell
ansible-playbook -i inventory --limit "aquarium-vps" --ask-vault-pass aquarium.yml
```

### Variables

Des variables sont définies par défaut pour tous les hosts. Il est possible de les surcharger via la ligne de commande (voir la [documentation Ansible](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#passing-variables-on-the-command-line)).

* `postgres_image_version` : version de l'[image docker Postgres](https://hub.docker.com/_/postgres) à utiliser
    Par défaut : `11`
* `postgres_db` : nom de la base de données
    Par défaut : `sonarqube`
* `postgres_user` : nom d'utilisateur pour la base de données
    Par défaut: encrypté
* `postgres_password` : mot de passe pour la base de données
    Par défaut: encrypté
* `docker_registry` : registry docker sur lequel se logger
    Par défaut : `registry.gitlab.com`
    Si la valeur n'est pas définie (à vide), il n'y a pas de login à un registry (l'image sera recherchée sur le registry par défaut de docker)
* `docker_registry_username` : nom d'utilisateur pour se logger sur le registry docker
    Par défaut : encrypté
    Optionnel si `docker_registry` n'est pas définie
* `docker_registry_password` : mot de passe pour se logger sur le registry docker
    Par défaut : encrypté
    Optionnel si `docker_registry` n'est pas définie
* `docker_image` : nom de l'image docker à utiliser
    Par défaut : `sebastienm4jplayground/docker/sonarqube`
* `docker_image_version` : version de l'image docker à utiliser
    Par défaut : `latest`
* `sonarqube_host_port` : port sur lequel exposer SonarQube
    Par défaut : `9000`
* `sonarqube_admin_password` : mot de passe du compte `admin` de SonarQube
    Par défaut : encrypté
